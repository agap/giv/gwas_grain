library(data.table)
data <- fread("ETM_2002_CC.txt",verbose = TRUE)

# Data summary
dim(data) # 3275 x 96
summary(data)

# Liste des individus
ind <- unique(data[,code])
  
# Description des variables ? Disponible dans le rapport de stage de Bassirou Sine (Zotero)
library(ggplot2)
ggplot(dataETM, aes(x = variable, y = value)) +            # Applying ggplot function
  geom_boxplot()




# ggplot(aes(x = State, y = Value, color = State))+
#   geom_boxplot() +
#   theme(axis.text.x = element_text(angle = 90, vjust = 0.3, hjust = 1)) +
#   theme(legend.position = "none",
#         axis.title.y = element_blank(),
#         axis.title.x = element_blank()) +
#   facet_wrap(.~Element)